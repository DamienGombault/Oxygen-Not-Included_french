Changelog :

v1.4.0 - 14-juin-2018 (CU-273433)
--------------------------------
Chaines traduites: 6838/6838 (100%)

* Traduction des chaines de la mise à jour « Cosmic Update »
* Changement de noms pour les constructions pour pallier les problèmes de place
  dans l'interface:
  - Fourniture -> Mobilier
  - Nourriture -> Aliments
  - Médecine -> Soins
  - Automatisation -> Automatisme (terme correct)
  - Utilitaires -> Divers
  - Livraison -> Convoi
* Corrections et reformulations d'anciennes traduction
* Changement de nom pour certaines créatures:
  - Luciole Vitale -> Luciole Énergique
  - Bouffette Dense -> Bouffette Compacte
  - Poisson Gulp -> Poisson Gobeur
  - Hatch -> Glouton

La discussion sur les traductions des noms de créatures se trouve ici:
https://forums.kleientertainment.com/topic/90444-l10n-help-wanted/

v1.3.1 - 05-mai-2018 (R2-266730)
--------------------------------
Chaines traduites: 6697/6697 (100%)
* Corrections et traductions de chaines pour les derniers hotfix de la mise à
  jour « Ranching II »

v1.3.0 - 03-mai-2018 (R2-266730)
--------------------------------
Chaines traduites: 6695/6695 (100%)

* Mise à jour et traduction des chaines de la mise à jour « Ranching II »
* Nouvelles traductions pour les créatures et plantes:
  - Bluff Briar: Fausse Bruyère
  - Muck Roots: Tubercule Boueux
  - Puft: Bouffette

Merci à Hasharin pour ses suggestions.

v1.1.0 - 16-fev-2018 (OC-255486)
--------------------------------
Chaines traduites: 5880/5880 (100%)

* Mise à jour pour le hotfix OC-255486
* Correction de la traduction pour une recette

v1.1.0 - 08-fev-2018
--------------------
Chaines traduites: 5876/5876 (100%)

* Mise à jour et traduction des chaines de la mise à jour « Occupational Upgrade »
* Encore beaucoup de corrections et d'adaptation des chaînes
* La 'Déconstruction' avec un détonateur d'explosifs n'était pas assez violente,
  place à la 'Démolition'
* Remplacement de "Décor" par "Décoration" dans les traductions. Elles sont
  nettement plus jolies ainsi
* Traduction de certaines recettes qui raviront vos papilles.
* Traduction de wheezewort

v1.0.0 - 05-jan-2018
--------------------
Chaînes traduites: 5517/5525 (99.85%)

* Importation des traductions existantes
* Beaucoup, Beaucoup de corrections, reformulations, corrections
